# distributed-task-queue

Simple local and cloud (using kubernetes) task queue.

The initial goal of this project was to launch on kubernetes (local or cloud instance) OR docker swarm (local or cloud instance) using the command "docker stack deploy --orchestrator=(kubernetes OR swarm)"
Unfortunately, the docker stack command requires the kubernetes instance to have the component "StackAPI". This component is only available on kubernetes clusters running with Docker EE, Docker for Windows and Docker for Mac.
This means no cloud stack using kubernetes and a regular kubernetes providers like Google gke and Azure aks.

We decided to go full kubernetes instead of using docker stack.

The major difference is that we use kubectl commands instead of docker stack.

Our initial docker-compose.yml file has to be translated to a kubernetes config file (kompose convert -o kubernetes-compose.yml). The docker-compose.yml file should be deprecated and updates could be done to kubernetes-compose.yml directly.

On all systems with docker, you can still start the stack with a commands similar to "docker swarm init && docker stack deploy -c docker-compose.yml mystackname". It will not autoscale.


### start_program.ps1

Working PowerShell example of a kubernetes deployment. It uses newtask.cs and worker.cs

The code was not written by a PowerShell or C# expert.

It is a fancy script basically running this with options:

    kubernetes create -f kubernetes-compose.yml
    # if cloud cluster, kubectl port-forward svc/queue-published 5672 15672
    dotnet run INPUT_NUMBER_HERE --project newtask



### newtask.cs

Based on rabbitmq examples.

Produces messages sent to the queue.

Receives input N.
Sends the following messages to the queue: {1, 2, 3, ... N}


### worker.cs

Based on rabbitmq examples.

Consumes messages from the queue and sleeps for (message + 10) seconds.

The worker is currently always alive and waits for message or does the work ("1 worker, many messages").
We could remove the infinite wait instruction from worker.cs and this would make our system "1 worker, 1 message".

Worker can be launched from any machine you have available that can connect to the cluster with kubectl. Simply port-forward 5672:5672 from the master instance.
For example, if running on a cloud cluster, you can port-forward to the service and start many workers on the local machine:

    kubectl port-forward svc/queue-published 5672 15672
    dotnet run --project worker

Local workers in docker containers can also be started in a similar way:

    kubectl port-forward svc/queue-published 5672 15672
    docker run --rm -it --network host -e QUEUE_HOSTNAME=localhost lpasselin/worker

The only difference is the queue consumer client must receive the proper hostname.

## Docker images

The queue uses a lightweight rabbitmq image.

The worker runs a csharp program. It parses a message from the queue and sleeps for the duration of the message content.
This program should be replaced with your own worker. The default image is lpasselin/worker (./worker/dockerfile)

Replace your worker image in kubernetes-compose.yml
Don't forget to push (if using a local image) to a public repository (or accessible to the nodes).


## docker-compose vs kubernetes config file

Initially we used docker-compose.yml to deploy using docker stack command.
Now that we use kubernetes, it is not required.

The kubernetes-compose.yml file can be easily generated from a docker-compose.yml file with kompose.

    # before running this, replace $WORKERS with a number in docker-compose.yml
    kompose convert -o kubernetes-compose.yml



--------------------

# Some useful commands

## Kubernetes

**I recommend using this GUI to understand kubernetes kubernetes.https://kubernetic.com/**
Or use the the gke GUI.

Also, https://github.com/ramitsurana/awesome-kubernetes

kubectl commands can also be used with a local stack started with docker stack deploy --orchestrator=kubernetes

### Delete everything on the cluster

    kubectl delete jobs,pods,services,deployments,hpa --all

### Wait for workers to be ready with connection to queue (they restart if unable to connect to queue)

    kubectl rollout status deployment/worker

### logs

I suggest using kubernetic for beginners.
Or use the dashboard. See https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/

cli option is poor for logs:

    # full cluster information, including logs
    kubectl cluster-info dump

    kubectl logs deployment/worker -f
    kubectl logs deployment/queue -f


## Docker Stack (swarm and local k8s only)

kubectl commands can also be used with a local stack started with docker stack deploy --orchestrator=kubernetes


### Start/Remove stack

    # (replace mystack with any name)
    docker stack deploy --compose-file docker-compose.yml mystack
    docker stack rm mystack


### logs

    # queue logs cmd. (dashboard available at 127.0.0.1:15672 on stack start)
    docker service logs mystack_queue -f
    # worker logs cmd
    docker service logs mystack_worker -f

Replace your worker image in docker-compose.yml, build and push with

    docker-compose build && docker-compose push



