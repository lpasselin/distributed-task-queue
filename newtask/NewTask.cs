﻿using System;
using RabbitMQ.Client;
using System.Text;

class NewTask
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Usage: run with input number as n_messages to send. ex: dotnet run 12 would send {1, 2, 3,  ... 12}");
        Console.WriteLine("Connection to queue...");
        // TODO confirm service exists
        var factory = new ConnectionFactory() { HostName = "127.0.0.1" };
        using(var connection = factory.CreateConnection())
        using(var channel = connection.CreateModel())
        {
            Console.WriteLine("Start send messages...");
            channel.QueueDeclare(queue: "input_queue", durable: true, exclusive: false, autoDelete: false, arguments: null);
            int maxNumber = Int32.Parse(args[0]);
            Console.WriteLine("Should send {0} messages", maxNumber);

            var properties = channel.CreateBasicProperties();
            properties.Persistent = true;
            for(int i=1; i<=maxNumber; i++){
                var message = i.ToString();
                var body = Encoding.UTF8.GetBytes(message);
                Console.WriteLine("Sent {0}", message);
                channel.BasicPublish(exchange: "", routingKey: "input_queue", basicProperties: properties, body: body);
            }
        }

        Console.WriteLine("All messages send!");
    }
}
