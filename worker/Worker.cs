﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading;

class Worker
{
    public static void Main()
    {
        string queueHostname = Environment.GetEnvironmentVariable("QUEUE_HOSTNAME");
        if (queueHostname == null) queueHostname = "localhost";
        var factory = new ConnectionFactory() { HostName = queueHostname };

        using(var connection = factory.CreateConnection())
        using(var channel = connection.CreateModel())
        {
            // if queue not found, worker/pod will exit with error.
            channel.QueueDeclare(queue: "input_queue", durable: true, exclusive: false, autoDelete: false, arguments: null);
            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine("Received message content: {0}", message);
                Console.WriteLine("Sleeping for {0}+10 seconds", message);

                int sleepSeconds = Int32.Parse(message);
                Thread.Sleep(sleepSeconds*1000 + 10000);
                Console.WriteLine("Done.");

                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };
            // confirm reception and processing done.
            channel.BasicConsume(queue: "input_queue", autoAck: false, consumer: consumer);

            // we could send output data to a second queue, which could be received by the client.

            Console.WriteLine("Waiting for messages...");
            Console.ReadLine(); // replace with proper csharp wait rabbitmq message

            var infiniteWait = new ManualResetEvent(false);
            infiniteWait.WaitOne();
        }
    }
}
