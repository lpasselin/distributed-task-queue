param(
    [int]$workers = $( Read-Host "Number of workers?" ),
    [int]$n_messages = $( Read-Host "Number of messages to send?" ),
    [string]$stack_name = "mystack",
    [int]$local_vs_cloud_threshold = 4,
    [string]$orchestrator = "kubernetes",
    [string]$local_kubernetes_context = "docker-for-desktop",
    # TODO replace this hardcoded value, see bottom of file
    [string]$cloud_kubernetes_context = "gke_credible-skill-215417_northamerica-northeast1-a_fpinnovations-test-cluster-1",
    [int]$sleep_time = 5
)
$ErrorActionPreference = "Stop"


# TODO autoscale worker count with kubernetes and simply select cloud or local?

# Select cloud of local and switch default cluster
$cloud = $workers -gt $local_vs_cloud_threshold
echo "value: $cloud"
$cluster_context = If ($cloud) {$cloud_kubernetes_context} Else {$local_kubernetes_context}
echo "Deployment on cluster: $cluster_context"
kubectl config use-context $cluster_context

echo "Deploying stack..."
# docker stack deploy --compose-file docker-compose.yml $stack_name
kubectl replace -f kubernetes-compose.yml --force
kubectl rollout status deployment/queue
kubectl rollout status deployment/worker # waiting for workers alive (they die if the queue is unavailable)
Start-Sleep -s $sleep_time

echo "Updating autoscaling parameters (setting number of workers)..."
# TODO set autoscaling better minimum and maximum values
kubectl delete hpa/worker
kubectl autoscale deployment worker --min=$workers --max=$workers --cpu-percent=80

echo "Port forward in background to allow discovery of queue service ports by local machine."
echo "If error but port-forward persists, kill port-forward with Get-Job |  Stop-Job"
$port_forward_job = Start-Job -ScriptBlock {kubectl port-forward service/queue 15672 5672}
Start-Sleep -s 4 # TODO check if port is avaible instead of sleeping

echo "Sending messages to queue..."
#TODO error management (if connection to queue before service is ready)
# It is possible to run this command in a docker.
dotnet run $n_messages --project=newtask

echo "Stack is running!"
echo "See queue information with guest:guest at http://127.0.0.1:15672"
echo "queue logs: kubectl logs deployment/queue -f"
echo "worker logs: kubectl logs deployment/worker -f"
echo "To delete everything on the cluster: kubectl delete jobs,pods,services,deployments,hpa --all"
echo "Don't forget to cleanup the cluster!"

#if ($cloud) {
echo "Keep this terminal window open to access queue management UI. Port-map must stay alive for cluster communication."
$port_forward_job | Wait-Job
#}
echo "Finished."


# TODO select context with help of kubectl
# get available kubernetes contexts.
# $kout = & kubectl config get-contexts --output=name
# $available_contexts = $kout.Split([Environment]::NewLine)
# if ($available_contexts[0].equals('docker-for-desktop') {
#     $cloud_kubernetes_context = $available_contexts[1]
# } else {
#    $cloud_kubernetes_context = $available_contexts[0]
# }
